package com.lumos.iplearner.controller;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
public class IPController {

    String ipAddress;
    String wifiName;

    @PostMapping("/ip")
    public String postIP(@RequestParam HashMap<String, String> request){
        ipAddress = request.get("ip_address");
        return "OK";
    }

    @GetMapping("/ipBackend/{ipAddress}")
    public String postIPBackend(@PathVariable String ipAddress){
        wifiName = ipAddress;
        System.out.println("Backend ip" + wifiName);
        return "Backend ip alindi";
    }

    @GetMapping("/ip")
    public String getIP(){
        return ipAddress + ":" + wifiName;
    }
}
